class User < ApplicationRecord
  validates :name, :phone, presence: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }, presence: true, uniqueness: true
  def identify
    "#{name} - #{email}"
  end
end
